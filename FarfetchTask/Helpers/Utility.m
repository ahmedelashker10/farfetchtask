//
//  Utility.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/30/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+ (Utility *)sharedInstance {
    static dispatch_once_t predicate = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&predicate, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

+ (NSMutableArray *)getFavouriteCharacters {
    NSData *favsData = [[NSUserDefaults standardUserDefaults] objectForKey:@"favs"];
    NSMutableArray *favChars = [NSKeyedUnarchiver unarchiveObjectWithData:favsData];
    if (favChars == nil) {
        favChars = [[NSMutableArray alloc] init];
    }
    return favChars;
}

+ (void)favouriteCharacter:(Character *)character {
    NSMutableArray *newFavs = [Utility getFavouriteCharacters];
    [newFavs addObject:character];
    NSData *favsData = [NSKeyedArchiver archivedDataWithRootObject:newFavs];
    [[NSUserDefaults standardUserDefaults] setObject:favsData forKey:@"favs"];
}

+ (void)unfavouriteCharacterID:(Character *)character {
    NSMutableArray *newFavs = [Utility getFavouriteCharacters];
    
    Character *toBeRemoved = nil;
    for (Character *fav in newFavs) {
        if([fav.name isEqualToString:character.name]) {
            toBeRemoved = fav;
        }
    }
    if(toBeRemoved)
        [newFavs removeObject:toBeRemoved];
    
    NSData *favsData = [NSKeyedArchiver archivedDataWithRootObject:newFavs];
    [[NSUserDefaults standardUserDefaults] setObject:favsData forKey:@"favs"];
}

+ (BOOL)isFavouriteCharacter:(Character *)character {
    NSMutableArray *tempFavs = [Utility getFavouriteCharacters];
    for (Character *fav in tempFavs) {
        if([fav.name isEqualToString:character.name]) {
            return YES;
        }
    }
    return NO;
}

@end
