//
//  Utility.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/30/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Character.h"

@interface Utility : NSObject

+ (Utility *) sharedInstance;
+ (NSMutableArray *)getFavouriteCharacters;
+ (void)favouriteCharacter:(Character *)character;
+ (void)unfavouriteCharacterID:(Character *)character;
+ (BOOL)isFavouriteCharacter:(Character *)character;

@end
