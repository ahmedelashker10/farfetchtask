//
//  NSString+MD5.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//
// Courtesy to Grant Paul's Stackoverflow Answer On How To Create MD5 Hash Of String (https://stackoverflow.com/questions/2018550/how-do-i-create-an-md5-hash-of-a-string-in-cocoa?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa)

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end
