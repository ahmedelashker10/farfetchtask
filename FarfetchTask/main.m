//
//  main.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/25/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
