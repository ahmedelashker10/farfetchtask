//
//  ItemCell.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/29/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;

@end
