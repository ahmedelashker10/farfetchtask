//
//  DetailsViewController.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/28/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "DetailsViewController.h"
#import "ItemCell.h"
#import "Utility.h"

@interface DetailsViewController () <UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UILabel *lblSuperheroName;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewCharacter;

@property (strong, nonatomic) IBOutlet UITableView *tableViewComics;
@property (strong, nonatomic) IBOutlet UITableView *tableViewEvents;
@property (strong, nonatomic) IBOutlet UITableView *tableViewSeries;
@property (strong, nonatomic) IBOutlet UITableView *tableViewStories;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightTableComics;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightTableEvents;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightTableSeries;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightTableStories;

@property (strong, nonatomic) IBOutlet UIButton *btnFavourite;
@property (strong, nonatomic) IBOutlet UIButton *btnComics;
@property (strong, nonatomic) IBOutlet UIButton *btnEvents;
@property (strong, nonatomic) IBOutlet UIButton *btnSeries;
@property (strong, nonatomic) IBOutlet UIButton *btnStories;

@end

@implementation DetailsViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self registerCellNibs];
    [self collapseTables];
    [self deselectButtons];
    [self setHeaderImage];
    [self checkFavourite];
    
    [_lblSuperheroName setText:_character.name];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)registerCellNibs {    
    [_tableViewComics registerNib:[UINib nibWithNibName:@"ItemCell" bundle:nil] forCellReuseIdentifier:@"itemCell"];
    [_tableViewEvents registerNib:[UINib nibWithNibName:@"ItemCell" bundle:nil] forCellReuseIdentifier:@"itemCell"];
    [_tableViewSeries registerNib:[UINib nibWithNibName:@"ItemCell" bundle:nil] forCellReuseIdentifier:@"itemCell"];
    [_tableViewStories registerNib:[UINib nibWithNibName:@"ItemCell" bundle:nil] forCellReuseIdentifier:@"itemCell"];
}

- (void)collapseTables {
    _constraintHeightTableComics.constant = 0;
    _constraintHeightTableEvents.constant = 0;
    _constraintHeightTableSeries.constant = 0;
    _constraintHeightTableStories.constant = 0;
}

- (void)deselectButtons {
    [_btnComics setSelected:NO];
    [_btnEvents setSelected:NO];
    [_btnSeries setSelected:NO];
    [_btnStories setSelected:NO];
}

- (void)setHeaderImage {
    dispatch_queue_t downloadQueue = dispatch_queue_create("image downloader", NULL);
    dispatch_async(downloadQueue, ^{
        
        NSString *imgURL = [NSString stringWithFormat:@"%@.%@", _character.thumbnail.path, _character.thumbnail.extension];
        NSURL *url = [NSURL URLWithString:imgURL];
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        UIImage *image = [UIImage imageWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _imgViewCharacter.image = image;
        });
    });
}

- (void)checkFavourite {
    if([Utility isFavouriteCharacter:_character]) {
        _btnFavourite.selected = YES;
        [_btnFavourite setTitle:@"Unfavourite" forState:UIControlStateSelected];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == _tableViewComics)
        return _comics.count;
    else if(tableView == _tableViewEvents)
        return _events.count;
    else if(tableView == _tableViewSeries)
        return _series.count;
    else
        return _stories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"itemCell" forIndexPath:indexPath];
    if(cell) {
        if(tableView == _tableViewComics)
            [self fillCell:cell fromMarvelModel:[_comics objectAtIndex:indexPath.row]];
        else if(tableView == _tableViewEvents)
            [self fillCell:cell fromMarvelModel:[_events objectAtIndex:indexPath.row]];
        else if(tableView == _tableViewSeries)
            [self fillCell:cell fromMarvelModel:[_series objectAtIndex:indexPath.row]];
        else
            [self fillCell:cell fromMarvelModel:[_stories objectAtIndex:indexPath.row]];
    }
    return cell;
}

- (void)fillCell:(ItemCell*)cell fromMarvelModel:(MarvelModel*)model {
    NSString *title = @"";
    NSString *description = @"";
    
    if([model.title class] != [NSNull class])
        title = model.title;
    
    if([model.desc class] != [NSNull class])
        description = model.desc;
    
    [cell.lblName setText:title];
    [cell.lblDesc setText:description];
}

#pragma mark - Action Handlers
- (IBAction)btnFavouriteTapped:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if([self toggleSenderButtonSelection:sender]) {
        [Utility favouriteCharacter:_character];
        [_btnFavourite setTitle:@"Unfavourite" forState:UIControlStateSelected];
    }
    else {
        [Utility unfavouriteCharacterID:_character];
        [_btnFavourite setTitle:@"Favourite" forState:UIControlStateNormal];
    }
}

- (IBAction)btnComicsTapped:(id)sender {
    if([self toggleSenderButtonSelection:sender]) {
        [_tableViewComics reloadData];
        _constraintHeightTableComics.constant = [self calculateCollectionTextsHeights:_comics];
    }
}

- (IBAction)btnEventsTapped:(id)sender {
    if([self toggleSenderButtonSelection:sender]) {
        [_tableViewEvents reloadData];
        _constraintHeightTableEvents.constant = [self calculateCollectionTextsHeights:_events];
    }
}

- (IBAction)btnSeriesTapped:(id)sender {
    if([self toggleSenderButtonSelection:sender]) {
        [_tableViewSeries reloadData];
        _constraintHeightTableSeries.constant = [self calculateCollectionTextsHeights:_series];
    }
}

- (IBAction)btnStoriesTapped:(id)sender {
    if([self toggleSenderButtonSelection:sender]) {
        [_tableViewStories reloadData];
        _constraintHeightTableStories.constant = [self calculateCollectionTextsHeights:_stories];
    }
}

- (BOOL)toggleSenderButtonSelection:(id)sender {
    UIButton *btn = (UIButton*)sender;
    bool selection = btn.selected;
    
    [self collapseTables];
    [self deselectButtons];
    
    btn.selected = !selection;
    return btn.selected;
}

- (CGFloat)calculateCollectionTextsHeights:(NSMutableArray*)collection {
    CGFloat titleHeight = 0;
    CGFloat descHeight = 0;
    CGFloat marginsHeights = 24 * collection.count;
    
    for (MarvelModel *model in collection) {
        if([model.title class] != [NSNull class]) {
            titleHeight += [self getStringHeight:model.title];
        }
        
        if([model.desc class] != [NSNull class]) {
            titleHeight += [self getStringHeight:model.desc];
        }
    }
    
    return (titleHeight + descHeight + marginsHeights);
}

- (CGFloat)getStringHeight:(NSString*)string {
    NSAttributedString *attributedText =
    [[NSAttributedString alloc] initWithString:string
                                    attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17.0f]}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.view.frame.size.width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;
    return size.height;

}
@end
