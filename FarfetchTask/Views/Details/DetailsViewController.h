//
//  DetailsViewController.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/28/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Character.h"

@interface DetailsViewController : UIViewController

@property (nonatomic, strong) Character *character;
@property (nonatomic, strong) NSMutableArray *comics;
@property (nonatomic, strong) NSMutableArray *events;
@property (nonatomic, strong) NSMutableArray *series;
@property (nonatomic, strong) NSMutableArray *stories;

@end
