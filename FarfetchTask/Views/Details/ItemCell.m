//
//  ItemCell.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/29/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "ItemCell.h"

@implementation ItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
