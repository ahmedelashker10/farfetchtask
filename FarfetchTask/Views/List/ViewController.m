//
//  ViewController.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/25/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "ViewController.h"
#import "BackendHandler.h"
#import "Character.h"
#import "SuperHeroCell.h"
#import "BackendParameters.h"
#import "DetailsViewController.h"
#import "Utility.h"

@interface ViewController () <BackendDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (strong, nonatomic) IBOutlet UILabel *lblLoading;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIButton *btnShowFavourites;

@property (nonatomic, strong) NSMutableArray *characters;
@property (nonatomic, strong) NSMutableArray *searchResultsCharacters;
@property (nonatomic, strong) BackendHandler *service;
@property (nonatomic, strong) DetailsViewController *detailsVC;

@property (nonatomic, strong) Character *lastSelectedCharacter;
@property (nonatomic, strong) NSDictionary *imageCache;

@property NSInteger comicsCopyIndex;
@property NSInteger eventsCopyIndex;
@property NSInteger seriesCopyIndex;
@property NSInteger storiesCopyIndex;

@property bool isSearching;

@end

@implementation ViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _characters = [[NSMutableArray alloc] init];
    _searchResultsCharacters = [[NSMutableArray alloc] init];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"SuperHeroCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    
    _service = [[BackendHandler alloc] init];
    _service.delegate = self;
    
    [self getCharacters];
}

- (void)getCharacters {
    _isSearching = NO;
    [_service getCharactersWithParameters:[NSString stringWithFormat:@"%@%lu", paramOffset, (unsigned long)_characters.count]];
    [_lblLoading setText:@"Loading Characters"];
    [_loadingView setHidden:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [_collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Backend Delegate
- (void)backendDidRespondWithArray:(NSArray*)array function:(NSString*)functionName {
    
    if([functionName isEqualToString:svcCharacters]) {
        if(_isSearching)
            _searchResultsCharacters = [NSMutableArray arrayWithArray:array];
        else
            [_characters addObjectsFromArray:array];
        
        dispatch_async (dispatch_get_main_queue(), ^{
            [_collectionView reloadData];
            [_loadingView setHidden:YES];
        });
    }
    else if([functionName containsString:svcComics]) {
        
        [_detailsVC.comics addObjectsFromArray:array];
        _comicsCopyIndex++;
        
        if(_comicsCopyIndex == 3 || _comicsCopyIndex >= _lastSelectedCharacter.comics.items.count) {
            dispatch_async (dispatch_get_main_queue(), ^{
                if(_lastSelectedCharacter.events.items.count > 0) {
                    [_lblLoading setText:@"Loading Events"];
                    [self loadLastSelectedCharacterEventsAccordingToCopyIndex];
                }
                else if(_lastSelectedCharacter.series.items.count > 0) {
                    [_lblLoading setText:@"Loading Series"];
                    [self loadLastSelectedCharacterSeriesAccordingToCopyIndex];
                }
                else if(_lastSelectedCharacter.stories.items.count > 0) {
                    [_lblLoading setText:@"Loading Stories"];
                    [self loadLastSelectedCharacterStoriesAccordingToCopyIndex];
                }
                else {
                    [_loadingView setHidden:YES];
                    [self.navigationController pushViewController:_detailsVC animated:YES];
                }
            });
        }
        else {
            [self loadLastSelectedCharacterComicsAccordingToCopyIndex];
        }
    }
    else if([functionName containsString:svcEvents]) {
        
        [_detailsVC.events addObjectsFromArray:array];
        _eventsCopyIndex++;
        
        if(_eventsCopyIndex == 3 || _eventsCopyIndex >= _lastSelectedCharacter.events.items.count) {
            dispatch_async (dispatch_get_main_queue(), ^{
                if(_lastSelectedCharacter.series.items.count > 0) {
                    [_lblLoading setText:@"Loading Series"];
                    [self loadLastSelectedCharacterSeriesAccordingToCopyIndex];
                }
                else if(_lastSelectedCharacter.stories.items.count > 0) {
                    [_lblLoading setText:@"Loading Stories"];
                    [self loadLastSelectedCharacterStoriesAccordingToCopyIndex];
                }
                else {
                    [_loadingView setHidden:YES];
                    [self.navigationController pushViewController:_detailsVC animated:YES];
                }
            });
        }
        else {
            [self loadLastSelectedCharacterEventsAccordingToCopyIndex];
        }
    }
    else if([functionName containsString:svcSeries]) {
        
        [_detailsVC.series addObjectsFromArray:array];
        _seriesCopyIndex++;
        
        if(_seriesCopyIndex == 3 || _seriesCopyIndex >= _lastSelectedCharacter.series.items.count) {
            dispatch_async (dispatch_get_main_queue(), ^{
                if(_lastSelectedCharacter.events.items.count > 0) {
                    [_lblLoading setText:@"Loading Stories"];
                    [self loadLastSelectedCharacterStoriesAccordingToCopyIndex];
                }
                else {
                    [_loadingView setHidden:YES];
                    [self.navigationController pushViewController:_detailsVC animated:YES];
                }
            });
        }
        else {
            [self loadLastSelectedCharacterSeriesAccordingToCopyIndex];
        }
    }
    else if([functionName containsString:svcStories]) {
        
        [_detailsVC.stories addObjectsFromArray:array];
        _storiesCopyIndex++;
        
        if(_storiesCopyIndex == 3 || _storiesCopyIndex >= _lastSelectedCharacter.stories.items.count) {
            dispatch_async (dispatch_get_main_queue(), ^{
                [_loadingView setHidden:YES];
                [self.navigationController pushViewController:_detailsVC animated:YES];
            });
        }
        else {
            [self loadLastSelectedCharacterStoriesAccordingToCopyIndex];
        }
    }
}

- (void)backendDidFailWithError:(NSString *)error {
    dispatch_async (dispatch_get_main_queue(), ^{
        [_loadingView setHidden:YES];
        [self showErrorAlertWithMessage:error];
    });
}

- (void)showErrorAlertWithMessage:(NSString*)message {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Collection View Data Source
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(_isSearching) {
        if(_searchResultsCharacters.count == 0)
            [self showErrorAlertWithMessage:@"No results found"];
        return _searchResultsCharacters.count;
    }
    else if (_btnShowFavourites.selected) {
        if([Utility getFavouriteCharacters].count == 0)
            [self showErrorAlertWithMessage:@"No favourites found"];
        return [Utility getFavouriteCharacters].count;
    }
    else
        return _characters.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SuperHeroCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (cell) {
        
        Character *character;
        if(_isSearching)
            character = [_searchResultsCharacters objectAtIndex:indexPath.row];
        else if (_btnShowFavourites.selected)
            character = [[Utility getFavouriteCharacters] objectAtIndex:indexPath.row];
        else
            character = [_characters objectAtIndex:indexPath.row];
        
        NSString *imgURL = [NSString stringWithFormat:@"%@.%@", character.thumbnail.path, character.thumbnail.extension];
        UIImage *cachedImage = [_imageCache objectForKey:imgURL];
        
        if(cachedImage) {
            cell.imgSuperHero.image = cachedImage;
        }
        else {
            cell.imgSuperHero.image = nil;
            
            dispatch_queue_t downloadQueue = dispatch_queue_create("image downloader", NULL);
            dispatch_async(downloadQueue, ^{
                
                NSString *imgURL = [NSString stringWithFormat:@"%@.%@", character.thumbnail.path, character.thumbnail.extension];
                NSURL *url = [NSURL URLWithString:imgURL];
                NSData *data = [NSData dataWithContentsOfURL:url];
                
                UIImage *image = [UIImage imageWithData:data];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.imgSuperHero.image = image;
                });
                
                [_imageCache setValue:image forKey:imgURL];
            });
        }
        
        [cell.lblSuperHeroName setText:character.name];
        
        return cell;
    }
    
    return cell;
}

#pragma mark - Collection View Delegate Flow Layout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((self.view.frame.size.width - 10) / 2, ((self.view.frame.size.width - 10) / 2) * 1.5);
}

#pragma mark - Collection View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(_isSearching)
        return;
    
    if ([scrollView isKindOfClass:[UICollectionView class]] == YES) {
        if (self.collectionView.contentOffset.y >= (self.collectionView.contentSize.height - self.collectionView.bounds.size.height)) {
            if(scrollView.isDragging && _loadingView.isHidden) {
                [_loadingView setHidden:NO];
                [self getCharacters];
            }
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Character *character;
    if(_isSearching)
        character = [_searchResultsCharacters objectAtIndex:indexPath.row];
    else if (_btnShowFavourites.selected)
        character = [[Utility getFavouriteCharacters] objectAtIndex:indexPath.row];
    else
        character = [_characters objectAtIndex:indexPath.row];
    _lastSelectedCharacter = character;
    _detailsVC = [[DetailsViewController alloc] init];
    _detailsVC.character = _lastSelectedCharacter;
    
    [self resetDetailsVCCollections];
    [self resetCopyIndexes];
    
    if(_lastSelectedCharacter.comics.items.count > 0) {
        [_lblLoading setText:@"Loading Comics"];
        [_loadingView setHidden:NO];
        [self loadLastSelectedCharacterComicsAccordingToCopyIndex];
    }
    else if(_lastSelectedCharacter.events.items.count > 0) {
        [_lblLoading setText:@"Loading Events"];
        [_loadingView setHidden:NO];
        [self loadLastSelectedCharacterEventsAccordingToCopyIndex];
    }
    else if(_lastSelectedCharacter.series.items.count > 0) {
        [_lblLoading setText:@"Loading Series"];
        [_loadingView setHidden:NO];
        [self loadLastSelectedCharacterSeriesAccordingToCopyIndex];
    }
    else if(_lastSelectedCharacter.stories.items.count > 0) {
        [_lblLoading setText:@"Loading Stories"];
        [_loadingView setHidden:NO];
        [self loadLastSelectedCharacterStoriesAccordingToCopyIndex];
    }
    else {
        [self.navigationController pushViewController:_detailsVC animated:YES];
    }
}

- (void)resetDetailsVCCollections {
    _detailsVC.comics = [[NSMutableArray alloc] init];
    _detailsVC.events = [[NSMutableArray alloc] init];
    _detailsVC.series = [[NSMutableArray alloc] init];
    _detailsVC.stories = [[NSMutableArray alloc] init];
}

- (void)resetCopyIndexes {
    _comicsCopyIndex = 0;
    _eventsCopyIndex = 0;
    _seriesCopyIndex = 0;
    _storiesCopyIndex = 0;
}

- (void)loadLastSelectedCharacterComicsAccordingToCopyIndex {
    // Note: At the end of each resource URI is the required ID to load the resource using it's corresponding web service
    NSString *comicID = [[[[_lastSelectedCharacter.comics.items objectAtIndex:_comicsCopyIndex] resourceURI] componentsSeparatedByString:@"/"] lastObject];
    [_service getComicsWithComicID:comicID];
}

- (void)loadLastSelectedCharacterEventsAccordingToCopyIndex {
    NSString *eventID = [[[[_lastSelectedCharacter.events.items objectAtIndex:_eventsCopyIndex] resourceURI] componentsSeparatedByString:@"/"] lastObject];
    [_service getEventsWithEventID:eventID];
}

- (void)loadLastSelectedCharacterSeriesAccordingToCopyIndex {
    NSString *serieID = [[[[_lastSelectedCharacter.series.items objectAtIndex:_seriesCopyIndex] resourceURI] componentsSeparatedByString:@"/"] lastObject];
    [_service getSeriesWithSerieID:serieID];
}

- (void)loadLastSelectedCharacterStoriesAccordingToCopyIndex {
    NSString *storyID = [[[[_lastSelectedCharacter.stories.items objectAtIndex:_storiesCopyIndex] resourceURI] componentsSeparatedByString:@"/"] lastObject];
    [_service getStoriesWithStoryID:storyID];
}

#pragma mark - Search Bar Delegate
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    
    [self reloadInitialCharacters];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if([searchText isEqualToString:@""] || searchText == nil)
        [self reloadInitialCharacters];
}

- (void)reloadInitialCharacters {
    _isSearching = NO;
    [_collectionView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    if([searchBar.text isEqualToString:@""] || searchBar.text == nil)
        return;
    
    _btnShowFavourites.selected = NO;
    _isSearching = YES;
    [_lblLoading setText:@"Searching"];
    [_loadingView setHidden:NO];
    [_service getCharactersWithParameters:[NSString stringWithFormat:@"%@%@", paramName, searchBar.text]];
}

#pragma mark - Action Handlers
- (IBAction)btnShowFavouritesTapped:(id)sender {
    UIButton *btn = (UIButton*)sender;
    btn.selected = !btn.selected;
    [_collectionView reloadData];
}

@end
