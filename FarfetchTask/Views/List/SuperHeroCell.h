//
//  SuperHeroCell.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperHeroCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgSuperHero;
@property (strong, nonatomic) IBOutlet UILabel *lblSuperHeroName;

@end
