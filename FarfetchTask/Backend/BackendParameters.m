//
//  BackendParameters.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/26/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "BackendParameters.h"

@implementation BackendParameters

NSString *const baseURL = @"https://gateway.marvel.com:443/v1/public/";
NSString *const apiPublicKey = @"0d5d6be43fc5c2a0f0948ab164f639b4";
NSString *const apiPrivateKey = @"aa04a5f61abe3fd008af5d7c840872268606d9bb";

NSString *const svcCharacters = @"characters";
NSString *const svcComics = @"comics";
NSString *const svcEvents = @"events";
NSString *const svcSeries = @"series";
NSString *const svcStories = @"stories";

NSString *const paramAnd = @"&";
NSString *const paramApiKey = @"apikey=";
NSString *const paramTS = @"ts=";
NSString *const paramHash = @"hash=";
NSString *const paramOffset = @"offset=";
NSString *const paramName = @"name=";

@end
