//
//  BackendHandler.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/25/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BackendDelegate <NSObject>

@optional
- (void)backendDidRespondWithDictionary:(NSDictionary*)dictionary function:(NSString*)functionName;
- (void)backendDidRespondWithArray:(NSArray*)array function:(NSString*)functionName;

@required
- (void)backendDidFailWithError:(NSString *)errorMessage;

@end

@interface BackendHandler : NSObject<NSURLConnectionDelegate> {
    NSString *function;
}

@property (nonatomic, weak) id <BackendDelegate> delegate;

- (void)getCharactersWithParameters:(NSString*)params;
- (void)getComicsWithComicID:(NSString*)comicID;
- (void)getEventsWithEventID:(NSString*)eventID;
- (void)getSeriesWithSerieID:(NSString*)serieID;
- (void)getStoriesWithStoryID:(NSString*)storyID;

@end
