//
//  Character.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "Character.h"
#import "URL.h"

@implementation Character

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if (dictionary) {
            if([dictionary valueForKey:@"id"])
                self.identifier = [dictionary valueForKey:@"id"];
            
            if([dictionary valueForKey:@"name"])
                self.name = [dictionary valueForKey:@"name"];
            
            if([dictionary valueForKey:@"description"])
                self.desc = [dictionary valueForKey:@"description"];
            
            if([dictionary valueForKey:@"modified"])
                self.modified = [dictionary valueForKey:@"modified"];
            
            if([dictionary valueForKey:@"thumbnail"])
                self.thumbnail = [[Thumbnail alloc] initWithJSONDictionary:[dictionary valueForKey:@"thumbnail"]];
            
            if([dictionary valueForKey:@"resourceURI"])
                self.resourceURI = [dictionary valueForKey:@"resourceURI"];
            
            if([dictionary valueForKey:@"comics"])
                self.comics = [[MarvelCollection alloc] initWithJSONDictionary:[dictionary valueForKey:@"comics"]];
            
            if([dictionary valueForKey:@"series"])
                self.series = [[MarvelCollection alloc] initWithJSONDictionary:[dictionary valueForKey:@"series"]];
            
            if([dictionary valueForKey:@"stories"])
                self.stories = [[MarvelCollection alloc] initWithJSONDictionary:[dictionary valueForKey:@"stories"]];
            
            if([dictionary valueForKey:@"events"])
                self.events = [[MarvelCollection alloc] initWithJSONDictionary:[dictionary valueForKey:@"events"]];
            
            if([dictionary valueForKey:@"urls"])
            {
                NSMutableArray *urls = [[NSMutableArray alloc] init];
                for (NSDictionary *item in [dictionary valueForKey:@"urls"]) {
                    URL *url = [[URL alloc] initWithJSONDictionary:item];
                    [urls addObject:url];
                }
                self.urls = urls;
            }
        }
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _name = [coder decodeObjectForKey:@"name"];
        _modified = [coder decodeObjectForKey:@"modified"];
        _thumbnail = [coder decodeObjectForKey:@"thumbnail"];
        _resourceURI = [coder decodeObjectForKey:@"resourceURI"];
        _comics = [coder decodeObjectForKey:@"comics"];
        _series = [coder decodeObjectForKey:@"series"];
        _stories = [coder decodeObjectForKey:@"stories"];
        _events = [coder decodeObjectForKey:@"events"];
        _urls = [coder decodeObjectForKey:@"urls"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeObject:_modified forKey:@"modified"];
    [aCoder encodeObject:_thumbnail forKey:@"thumbnail"];
    [aCoder encodeObject:_resourceURI forKey:@"resourceURI"];
    [aCoder encodeObject:_comics forKey:@"comics"];
    [aCoder encodeObject:_series forKey:@"series"];
    [aCoder encodeObject:_stories forKey:@"stories"];
    [aCoder encodeObject:_events forKey:@"events"];
    [aCoder encodeObject:_urls forKey:@"urls"];
}

@end
