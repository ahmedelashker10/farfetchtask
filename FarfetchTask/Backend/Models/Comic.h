//
//  Comic.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/28/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MarvelModel.h"

@interface Comic : MarvelModel

@property (nonatomic, strong) NSNumber *digitalId;
@property (nonatomic, strong) NSNumber *issueNumber;
@property (nonatomic, strong) NSString *variantDescription;

@end
