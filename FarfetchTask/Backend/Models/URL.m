//
//  URL.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "URL.h"

@implementation URL

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if (dictionary) {
            for (NSString* key in dictionary) {
                [self setValue:[dictionary valueForKey:key] forKey:key];
            }
        }
    }
    return self;
}

@end
