//
//  URL.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "MarvelModel.h"

@interface URL : MarvelModel

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *url;

@end
