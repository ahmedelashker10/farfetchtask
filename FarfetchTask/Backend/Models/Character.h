//
//  Character.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MarvelModel.h"
#import "Thumbnail.h"
#import "MarvelCollection.h"

@interface Character : MarvelModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *modified;
@property (nonatomic, strong) Thumbnail *thumbnail;
@property (nonatomic, strong) NSString *resourceURI;
@property (nonatomic, strong) MarvelCollection *comics;
@property (nonatomic, strong) MarvelCollection *series;
@property (nonatomic, strong) MarvelCollection *stories;
@property (nonatomic, strong) MarvelCollection *events;
@property (nonatomic, strong) NSArray *urls;

@end
