//
//  MarvelItem.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "MarvelItem.h"

@implementation MarvelItem

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if (dictionary) {
            for (NSString* key in dictionary) {
                [self setValue:[dictionary valueForKey:key] forKey:key];
            }
        }
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _resourceURI = [coder decodeObjectForKey:@"resourceURI"];
        _name = [coder decodeObjectForKey:@"name"];
        _type = [coder decodeObjectForKey:@"type"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_resourceURI forKey:@"resourceURI"];
    [aCoder encodeObject:_name forKey:@"name"];
    [aCoder encodeObject:_type forKey:@"type"];
}

@end
