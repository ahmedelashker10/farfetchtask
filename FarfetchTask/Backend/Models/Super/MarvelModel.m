//
//  MarvelModel.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "MarvelModel.h"

@implementation MarvelModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {
    return [self init];
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _identifier = [coder decodeObjectForKey:@"identifier"];
        _title = [coder decodeObjectForKey:@"title"];
        _desc = [coder decodeObjectForKey:@"desc"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_identifier forKey:@"identifier"];
    [aCoder encodeObject:_title forKey:@"title"];
    [aCoder encodeObject:_desc forKey:@"desc"];
}

@end
