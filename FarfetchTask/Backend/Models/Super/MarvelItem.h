//
//  MarvelItem.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "MarvelModel.h"

@interface MarvelItem : MarvelModel

@property (nonatomic, strong) NSString *resourceURI;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *type;

@end
