//
//  MarvelModel.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MarvelModel : NSObject <NSCoding>

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary;

@property (nonatomic, strong) NSNumber *identifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *desc;

@end
