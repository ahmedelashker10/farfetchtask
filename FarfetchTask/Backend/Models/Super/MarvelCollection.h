//
//  MarvelCollection.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "MarvelModel.h"

@interface MarvelCollection : MarvelModel

@property (nonatomic, strong) NSString *available;
@property (nonatomic, strong) NSString *collectionURI;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSNumber *returned;

@end
