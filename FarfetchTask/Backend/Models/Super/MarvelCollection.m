//
//  MarvelCollection.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "MarvelCollection.h"
#import "MarvelItem.h"

@implementation MarvelCollection

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if (dictionary) {
            for (NSString* key in dictionary) {
                [self setValue:[dictionary valueForKey:key] forKey:key];
            }
            
            if([dictionary valueForKey:@"items"])
            {
                NSMutableArray *marvelCollection = [[NSMutableArray alloc] init];
                for (NSDictionary *item in [dictionary valueForKey:@"items"]) {
                    MarvelItem *mi = [[MarvelItem alloc] initWithJSONDictionary:item];
                    [marvelCollection addObject:mi];
                }
                self.items = marvelCollection;
            }
        }
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _available = [coder decodeObjectForKey:@"available"];
        _collectionURI = [coder decodeObjectForKey:@"collectionURI"];
        _items = [coder decodeObjectForKey:@"items"];
        _returned = [coder decodeObjectForKey:@"returned"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_available forKey:@"available"];
    [aCoder encodeObject:_collectionURI forKey:@"collectionURI"];
    [aCoder encodeObject:_items forKey:@"items"];
    [aCoder encodeObject:_returned forKey:@"returned"];
}

@end
