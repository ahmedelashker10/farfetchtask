//
//  Thumbnail.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "Thumbnail.h"

@implementation Thumbnail

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if (dictionary) {
            for (NSString* key in dictionary) {
                [self setValue:[dictionary valueForKey:key] forKey:key];
            }
        }
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _path = [coder decodeObjectForKey:@"path"];
        _extension = [coder decodeObjectForKey:@"extension"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_path forKey:@"path"];
    [aCoder encodeObject:_extension forKey:@"extension"];
}

@end
