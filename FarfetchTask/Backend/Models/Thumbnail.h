//
//  Thumbnail.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/27/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "MarvelModel.h"

@interface Thumbnail : MarvelModel

@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSString *extension;

@end
