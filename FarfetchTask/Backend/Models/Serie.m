//
//  Serie.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/28/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "Serie.h"

@implementation Serie

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if (dictionary) {
            if([dictionary valueForKey:@"id"])
                self.identifier = [dictionary valueForKey:@"id"];
            
            if([dictionary valueForKey:@"title"])
                self.title = [dictionary valueForKey:@"title"];
            
            if([dictionary valueForKey:@"description"])
                self.desc = [dictionary valueForKey:@"description"];
        }
    }
    
    return self;
}

@end
