//
//  Comic.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/28/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "Comic.h"

@implementation Comic

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        if (dictionary) {
            if([dictionary valueForKey:@"id"])
                self.identifier = [dictionary valueForKey:@"id"];
            
            if([dictionary valueForKey:@"digitalId"])
                self.digitalId = [dictionary valueForKey:@"digitalId"];
            
            if([dictionary valueForKey:@"title"])
                self.title = [dictionary valueForKey:@"title"];
            
            if([dictionary valueForKey:@"issueNumber"])
                self.issueNumber = [dictionary valueForKey:@"issueNumber"];
            
            if([dictionary valueForKey:@"variantDescription"])
                self.variantDescription = [dictionary valueForKey:@"variantDescription"];
            
            if([dictionary valueForKey:@"description"])
                self.desc = [dictionary valueForKey:@"description"];
        }
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _digitalId = [coder decodeObjectForKey:@"digitalId"];
        _issueNumber = [coder decodeObjectForKey:@"issueNumber"];
        _variantDescription = [coder decodeObjectForKey:@"variantDescription"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_digitalId forKey:@"digitalId"];
    [aCoder encodeObject:_issueNumber forKey:@"issueNumber"];
    [aCoder encodeObject:_variantDescription forKey:@"variantDescription"];
}
@end
