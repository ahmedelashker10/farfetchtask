//
//  BackendParameters.h
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/26/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackendParameters : NSObject

FOUNDATION_EXPORT NSString *const baseURL;
FOUNDATION_EXPORT NSString *const apiPublicKey;
FOUNDATION_EXPORT NSString *const apiPrivateKey;

FOUNDATION_EXPORT NSString *const svcCharacters;
FOUNDATION_EXPORT NSString *const svcComics;
FOUNDATION_EXPORT NSString *const svcEvents;
FOUNDATION_EXPORT NSString *const svcSeries;
FOUNDATION_EXPORT NSString *const svcStories;

FOUNDATION_EXPORT NSString *const paramAnd;
FOUNDATION_EXPORT NSString *const paramApiKey;
FOUNDATION_EXPORT NSString *const paramTS;
FOUNDATION_EXPORT NSString *const paramHash;
FOUNDATION_EXPORT NSString *const paramOffset;
FOUNDATION_EXPORT NSString *const paramName;

@end
