//
//  BackendHandler.m
//  FarfetchTask
//
//  Created by Ahmed Elashker on 4/25/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

#import "BackendHandler.h"
#import "BackendParameters.h"
#import "NSString+MD5.h"
#import "Character.h"
#import "Comic.h"
#import "Event.h"
#import "Serie.h"
#import "Story.h"

@implementation BackendHandler

- (void)getCharactersWithParameters:(NSString*)params {
    [self get:svcCharacters parameters:params completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            
            NSHTTPURLResponse *httpResponse  = (NSHTTPURLResponse*)response;
            NSInteger statusCode = httpResponse.statusCode;
            
            NSError *error;
            NSMutableDictionary *innerJson = [NSJSONSerialization
                                              JSONObjectWithData:data options:kNilOptions error:&error
                                              ];
            
            if (statusCode == 200) {
                NSArray *results = [[innerJson valueForKey:@"data"] valueForKey:@"results"];
                if(results)
                {
                    NSMutableArray *characters = [[NSMutableArray alloc] init];
                    for (NSDictionary *character in results) {
                        Character *ch = [[Character alloc] initWithJSONDictionary:character];
                        [characters addObject:ch];
                    }
                    [self.delegate backendDidRespondWithArray:characters function:function];
                }
            }
            else {
                NSString *errorMessage = [innerJson valueForKey:@"message"];
                if(errorMessage == nil)
                    errorMessage = [innerJson valueForKey:@"status"];
                [self.delegate backendDidFailWithError:errorMessage];
            }
        }
        else if (error)
        {
            [self.delegate backendDidFailWithError:error.description];
        }
    }];
}

- (void)getComicsWithComicID:(NSString*)comicID {
    [self get:[NSString stringWithFormat:@"%@/%@", svcComics, comicID] parameters:@"" completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            
            NSHTTPURLResponse *httpResponse  = (NSHTTPURLResponse*)response;
            NSInteger statusCode = httpResponse.statusCode;
            
            NSError *error;
            NSMutableDictionary *innerJson = [NSJSONSerialization
                                              JSONObjectWithData:data options:kNilOptions error:&error
                                              ];
            
            if (statusCode == 200) {
                NSArray *results = [[innerJson valueForKey:@"data"] valueForKey:@"results"];
                if(results)
                {
                    NSMutableArray *comics = [[NSMutableArray alloc] init];
                    for (NSDictionary *comic in results) {
                        Comic *com = [[Comic alloc] initWithJSONDictionary:comic];
                        [comics addObject:com];
                    }
                    [self.delegate backendDidRespondWithArray:comics function:function];
                }
            }
            else {
                NSString *errorMessage = [innerJson valueForKey:@"message"];
                if(errorMessage == nil)
                    errorMessage = [innerJson valueForKey:@"status"];
                [self.delegate backendDidFailWithError:errorMessage];
            }
        }
        else if (error)
        {
            [self.delegate backendDidFailWithError:error.description];
        }
    }];
}

- (void)getEventsWithEventID:(NSString*)eventID {
    [self get:[NSString stringWithFormat:@"%@/%@", svcEvents, eventID] parameters:@"" completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            
            NSHTTPURLResponse *httpResponse  = (NSHTTPURLResponse*)response;
            NSInteger statusCode = httpResponse.statusCode;
            
            NSError *error;
            NSMutableDictionary *innerJson = [NSJSONSerialization
                                              JSONObjectWithData:data options:kNilOptions error:&error
                                              ];
            
            if (statusCode == 200) {
                NSArray *results = [[innerJson valueForKey:@"data"] valueForKey:@"results"];
                if(results)
                {
                    NSMutableArray *events = [[NSMutableArray alloc] init];
                    for (NSDictionary *event in results) {
                        Event *eve = [[Event alloc] initWithJSONDictionary:event];
                        [events addObject:eve];
                    }
                    [self.delegate backendDidRespondWithArray:events function:function];
                }
            }
            else {
                NSString *errorMessage = [innerJson valueForKey:@"message"];
                if(errorMessage == nil)
                    errorMessage = [innerJson valueForKey:@"status"];
                [self.delegate backendDidFailWithError:errorMessage];
            }
        }
        else if (error)
        {
            [self.delegate backendDidFailWithError:error.description];
        }
    }];
}

- (void)getSeriesWithSerieID:(NSString*)serieID {
    [self get:[NSString stringWithFormat:@"%@/%@", svcSeries, serieID] parameters:@"" completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            
            NSHTTPURLResponse *httpResponse  = (NSHTTPURLResponse*)response;
            NSInteger statusCode = httpResponse.statusCode;
            
            NSError *error;
            NSMutableDictionary *innerJson = [NSJSONSerialization
                                              JSONObjectWithData:data options:kNilOptions error:&error
                                              ];
            
            if (statusCode == 200) {
                NSArray *results = [[innerJson valueForKey:@"data"] valueForKey:@"results"];
                if(results)
                {
                    NSMutableArray *series = [[NSMutableArray alloc] init];
                    for (NSDictionary *serie in results) {
                        Serie *sre = [[Serie alloc] initWithJSONDictionary:serie];
                        [series addObject:sre];
                    }
                    [self.delegate backendDidRespondWithArray:series function:function];
                }
            }
            else {
                NSString *errorMessage = [innerJson valueForKey:@"message"];
                if(errorMessage == nil)
                    errorMessage = [innerJson valueForKey:@"status"];
                [self.delegate backendDidFailWithError:errorMessage];
            }
        }
        else if (error)
        {
            [self.delegate backendDidFailWithError:error.description];
        }
    }];
}

- (void)getStoriesWithStoryID:(NSString*)storyID {
    [self get:[NSString stringWithFormat:@"%@/%@", svcStories, storyID] parameters:@"" completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            
            NSHTTPURLResponse *httpResponse  = (NSHTTPURLResponse*)response;
            NSInteger statusCode = httpResponse.statusCode;
            
            NSError *error;
            NSMutableDictionary *innerJson = [NSJSONSerialization
                                              JSONObjectWithData:data options:kNilOptions error:&error
                                              ];
            
            if (statusCode == 200) {
                NSArray *results = [[innerJson valueForKey:@"data"] valueForKey:@"results"];
                if(results)
                {
                    NSMutableArray *stories = [[NSMutableArray alloc] init];
                    for (NSDictionary *story in results) {
                        Story *sto = [[Story alloc] initWithJSONDictionary:story];
                        [stories addObject:sto];
                    }
                    [self.delegate backendDidRespondWithArray:stories function:function];
                }
            }
            else {
                NSString *errorMessage = [innerJson valueForKey:@"message"];
                if(errorMessage == nil)
                    errorMessage = [innerJson valueForKey:@"status"];
                [self.delegate backendDidFailWithError:errorMessage];
            }
        }
        else if (error)
        {
            [self.delegate backendDidFailWithError:error.description];
        }
    }];
}

- (void)get:(NSString*)functionName parameters:(NSString*)params completionHandler:(void (^)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error))completionHandler {
    
    function = functionName;
    
    NSMutableString *tempParams = [[NSMutableString alloc] initWithString:params];
    tempParams = [self appendApiKey:tempParams];
    tempParams = [self appendTimeStamp:tempParams];
    tempParams = [self appendHash:tempParams];
    
    tempParams = (NSMutableString*)[tempParams stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSMutableURLRequest *request =
    [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?%@", baseURL, functionName, tempParams]] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:600];
    
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:completionHandler];
    [dataTask resume];
}

- (NSMutableString*)appendApiKey:(NSMutableString*)params {
    if(params.length > 0)
        [params appendString:paramAnd];
    
    [params appendString:paramApiKey];
    [params appendString:apiPublicKey];
    return params;
}

- (NSMutableString*)appendTimeStamp:(NSMutableString*)params {
    [params appendString:paramAnd];
    [params appendString:paramTS];
    [params appendString:[self timeStampString]];
    return params;
}

- (NSMutableString*)timeStampString {
    return [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]]];
}

- (NSMutableString*)appendHash:(NSMutableString*)params {
    NSString *hashBase = [NSString stringWithFormat: @"%@%@%@", [self timeStampString], apiPrivateKey, apiPublicKey];
    
    [params appendString:paramAnd];
    [params appendString:paramHash];
    [params appendString:[hashBase MD5String]];
    return params;
}

@end
