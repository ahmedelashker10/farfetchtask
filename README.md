# FarfetchTask
This is a demo app which was built as part of my interviewing process with Farfetch

# Platform
Objective-C

# Usage
The application uses Marvel's API to retrieve details about superheroes and their relevant comics, events, series and stories.

The application allows the user to search through superheroes using Marvel's api.

The application allows the user to hand pick favourite superheroes and to list them separately.

# Code Structure
The project is organized into a number of folders.

- Backend: Includes classes which are responsible of directly interacting with Marvel's API and handling its' responses.

- Categories: Includes one category for NSString which translates strings into MD5 hashes.

- Helpers: Includes a helper class which supports common method calls from anywhere in the app.

- Views: Includes implemented UI and code behind for listing and details views.
